const toggleMenuButton = document.querySelector('.btn-menu');
const menu = document.querySelector('.nav-options');

toggleMenuButton.addEventListener('click', () => {
    toggleMenuButton.classList.toggle('active');
    menu.classList.toggle('active');
})